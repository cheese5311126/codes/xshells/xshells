# xSHELLS

[![SQAaaS badge shields.io](https://img.shields.io/badge/sqaaas%20software-silver-silver)](https://eu.badgr.com/public/assertions/50zMEGa5Sje8-QbgW5nSCQ "SQAaaS silver badge achieved")

xSHELLS is a high performance simulation code for rotating incompressible flows and magnetic fields in spherical
geometries, including geodynamo simulations. Spherical harmonic transforms are combined with Finite Differences in
radius to obtain a fast time-stepper.

## Documentation

For further information, please visit the xSHELLS
[User Guide](https://nschaeff.bitbucket.io/xshells/manual.html).

## Developers

Nathanaël Schaeffer
ISTerre/CNRS 

## License

[CeCILL License](http://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible with GNU GPL): everybody is free to use, modify, contribute.

## Copyright
Copyright (c) 2010-2020 Centre National de la Recherche Scientifique. written by Nathanael Schaeffer (CNRS, ISTerre, Grenoble, France). XSHELLS is distributed under the open source CeCILL License (GPL compatible) located in the LICENSE file.


